import {  Injectable} from '@angular/core';
import {  HttpClient,  HttpHeaders} from '@angular/common/http';
import {RequestOptions} from '@angular/http';
import { WeatherService } from './weather.service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { MatDialog } from '@angular/material';
import { ErrorModalComponent } from '../error-modal/error-modal.component';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient, private weatherService: WeatherService, public dialog: MatDialog) {}



  apiKey ='	XNDoLAwhZe85NOKOam50dntzGMXRD7pV'

  searchCities(char) {
    if(char)
      return this.http.get(`https://dataservice.accuweather.com/locations/v1/cities/autocomplete?apikey=${this.apiKey}&q=${char}`)
  }

  getForeCastWeather(cityKey){
    var currentWeather = this.weatherService.citiesWeather[cityKey];
    var currentDate = new Date();
    var isCurrentForecast = currentWeather && currentDate.getDate() === (new Date(currentWeather[0].Date)).getDate();
    if(isCurrentForecast){
      return of(currentWeather);
    }
    // this.weatherService.citiesWeather[cityKey] = this.weatherForecasts;
    return this.http.get(`https://dataservice.accuweather.com/forecasts/v1/daily/5day/${cityKey}?apikey=${this.apiKey}`)
      .pipe(
        map((res:any) => {
        this.weatherService.citiesWeather[cityKey] = res.DailyForecasts;
        return this.weatherService.citiesWeather[cityKey];
      })
      )
  }

  openDialog(title, error): void {
      this.dialog.open(ErrorModalComponent, {
      width: '250px',
      data: {error, title}
    });
  }

}
