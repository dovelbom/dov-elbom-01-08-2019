import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  currentDate = new Date();
  citiesWeather = {};
  selectedCity = {LocalizedName:"Tel Aviv",Key:"215854"};
  isCelcius = false;


  constructor() { }

  toCelcius(number){
    return ((number-32)/1.8).toFixed();
  }
}
