import { Component, OnInit, Input } from '@angular/core';
import { WeatherService } from 'src/app/services/weather.service';

@Component({
  selector: 'hrl-forecast',
  templateUrl: './forecast.component.html',
  styleUrls: ['./forecast.component.scss']
})
export class ForecastComponent implements OnInit {

  @Input() forecast
  imagesUrl = 'https://developer.accuweather.com/sites/default/files/0{INDEX}-s.png';
  constructor(public weatherService: WeatherService) { }
  
  ngOnInit() {
    var d= new Date(this.forecast.Date)
    
  }

  


  formatDayName(date){
    return  new Date(date).toDateString().split(' ')[0];

  }

}
