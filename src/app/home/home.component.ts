import { Component, OnInit, Output, ViewChild, ElementRef, AfterViewInit, OnDestroy } from '@angular/core';
import { ApiService } from '../services/api.service';
import {FormControl} from '@angular/forms';
import { Observable, fromEvent, Subscription } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import { WeatherService } from '../services/weather.service';
@Component({
  selector: 'hrl-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit, AfterViewInit, OnDestroy {
  
  myControl = new FormControl();
  autoCompleteCities=[]
  weatherForecasts=[]
  search = '';
  sub: Subscription;
  @ViewChild('searchinput') input: ElementRef;
  constructor(public apiService:ApiService, public weatherService: WeatherService) { }

  ngOnInit() {
    this.getWeather(this.weatherService.selectedCity.Key);
  }

  ngAfterViewInit(){
    this.sub = fromEvent(this.input.nativeElement, 'keyup').pipe(
      debounceTime(1000)).subscribe(value => {
        console.log(value);
        this.searchCities();
      }
    );
  }

  ngOnDestroy() {
    if(this.sub){
      this.sub.unsubscribe();
    }
  }

  

  inputValidator(event: any) {
    const pattern = /[^a-zA-Z]/;
    let inputChar = String.fromCharCode(event.charCode);

    if (pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  searchCities(){
    
    this.apiService.searchCities(this.search).subscribe((res:any)=>{
      this.autoCompleteCities=res;
    });
  }

  changeCity(city){
    this.weatherService.selectedCity = city;
    console.log(city);
    this.getWeather(city.Key);

  }
  getFavoritesWeather(cities){

    var favorites_cities = localStorage.getItem('favorites_cities');
    for(let i in cities){
      var city = favorites_cities[city];
      if(city){
        this.getWeather(city.Key);
      }
    }
  }
  addToFavorites(city){
    var favorites_cities = JSON.parse(localStorage.getItem('favorites_cities')) || {};
    if(favorites_cities[city.LocalizedName])
      delete favorites_cities[city.LocalizedName];
    else favorites_cities[city.LocalizedName] = city.Key;
    localStorage.setItem('favorites_cities', JSON.stringify(favorites_cities));
  }

  isFavorite(city){
    var favorites_cities = JSON.parse(localStorage.getItem('favorites_cities'));
    return favorites_cities && !!favorites_cities[city.LocalizedName];
  }
  getWeather(cityKey){  
    this.apiService.getForeCastWeather(this.weatherService.selectedCity.Key).subscribe((weatherForecasts:any)=>{
      this.weatherForecasts = weatherForecasts;
    })
    
   
     
  }
  
  


}
