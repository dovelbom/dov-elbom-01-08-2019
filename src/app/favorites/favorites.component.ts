import { Component, OnInit, Input } from '@angular/core';
import { WeatherService } from '../services/weather.service';
import { ApiService } from '../services/api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'hrl-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.scss']
})
export class FavoritesComponent implements OnInit {

  favorites = JSON.parse(localStorage.getItem('favorites_cities')) || [];
  favorites_cities = [];
  imagesUrl = 'https://developer.accuweather.com/sites/default/files/0{INDEX}-s.png';
  constructor(private weatherService: WeatherService, private apiService: ApiService, private router: Router) { } 

  ngOnInit() {
    for(let cityName in this.favorites){
      var cityKey = this.favorites[cityName]
      if(this.weatherService.citiesWeather[cityKey]){
        this.favorites_cities.push({
          LocalizedName: cityName,
          Key: cityKey,
          value:  this.weatherService.citiesWeather[cityKey][0]
        });
      }
      else this.apiService.getForeCastWeather(cityKey).subscribe(weatherForecasts =>{
        this.favorites_cities.push({
          LocalizedName: cityName,
          Key: cityKey,
          value: weatherForecasts[0]
        });
      })
      console.log(this.favorites_cities)
    }
  }

  changeCity(city){
    this.weatherService.selectedCity = city;
    this.router.navigate(['/']);
  }
}
