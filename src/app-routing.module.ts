import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FavoritesComponent } from './app/favorites/favorites.component';
import { HomeComponent } from './app/home/home.component';


const routes: Routes = [
  { path: '', component:HomeComponent  },
  { path: 'favorites', component:FavoritesComponent }
  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
  ],
  declarations: [
    
  ]
})
export class AppRoutingModule { }
